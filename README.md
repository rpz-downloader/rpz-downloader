# rpz-downloader

rpz-downloader is bash script which downloads RPZ files from the Internet in order to use them in your DNS server configuration for blocking access to certain domains. This is particularly useful for blocking domains used for malware distribution and phishing. Currently two sources are set up in the configuration file: the RPZ file by [urlhaus.abuse.ch](https://urlhaus.abuse.ch), and the blocklist by the [COVID-19 Cyber Threat Coalition](https://www.cyberthreatcoalition.org/).

License
=======
rpz-downloader is licensed under the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).

Disclaimer
==========

This is work in progress and has only had limited testing. There is little documentation and also little error handling in the script. Only use this at your own risk. Improvements are welcome.

Quick installation notes
========================
Move the rpz-downloader script to /usr/local/bin and make it executable.  Save the configuration file as /etc/rpz-downloader/rpz-downloader.conf, and create /var/cache/rpz-downloader and /var/lib/rpz-downloader.
Move the rpz-downloader script to /usr/local/bin and make it executable.  Save the configuration file as /etc/rpz-downloader/rpz-downloader.conf. Create the system user and group rpz-downloader and create /var/cache/rpz-downloader and /var/lib/rpz-downloader, owned by user and group rpz-downloader. Then place the systemd service and timer in /etc/systemd/system, run systemctl daemon-reload, and systemctl enable rpz-downloader.timer.

Then you can refer to the the RPZ files in /var/lib/rpz-downloader in the configuration of your DNS server.
